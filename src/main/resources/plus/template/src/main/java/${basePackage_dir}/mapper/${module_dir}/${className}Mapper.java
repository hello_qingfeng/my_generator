<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basePackage}.mapper.${module};


import ${basePackage}.entity.${module}.${className};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

<#include "/java_description.include">
public interface ${className}Mapper extends BaseMapper<${className}> {
}
