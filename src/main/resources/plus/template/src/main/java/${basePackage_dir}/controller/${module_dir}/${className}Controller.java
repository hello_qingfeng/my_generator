<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign resultEntityNameLower = resultEntityName?uncap_first>
package ${basePackage}.controller.${module};

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${basePackage}.entity.${module}.${className};
import ${basePackage}.service.${module}.${className}Service;
import com.lmc.business.api.web.response.ApiResponseEntity;
<#if showSwagger == 'true'>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
</#if>

<#include "/java_description.include">
<#if showSwagger == 'true'>
@Api(tags = "${controllerRemark}")
@ApiSupport(author = "${userName}")
</#if>
@RestController
@RequestMapping("${classNameLower}")
public class ${className}Controller {

    @Autowired
    private ${className}Service ${classNameLower}Service;

    /**
     * 查询列表
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("查询列表")
    </#if>
    @RequestMapping(value = "list", method = { RequestMethod.POST, RequestMethod.GET })
    public ApiResponseEntity list(HttpServletRequest request, ${className} entity) {
        Page page = new Page();
        page.setCurrent(current);
        page.setSize(size);
        return ApiResponseEntity.success(${classNameLower}Service.(page,Wrappers.emptyWrapper()));
    }

    /**
     * 查询单个详情
     *
     * @param id
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("查询单个详情")
    </#if>
    @RequestMapping(value = "info", method = { RequestMethod.POST, RequestMethod.GET })
    public ApiResponseEntity info(@RequestParam Integer id) {
        return ApiResponseEntity.success(${classNameLower}Service.queryById(id));
    }

    /**
     * 添加
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("添加")
    </#if>
    @RequestMapping(value = "insert", method = { RequestMethod.POST })
    public ApiResponseEntity insert(HttpServletRequest request, ${className} entity) {
        int r = ${classNameLower}Service.insert(entity);
        if(r == 0) {
            return ApiResponseEntity.fail("添加失败");
        }
        return ApiResponseEntity.success();
    }

    /**
     * 更新
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("更新")
    </#if>
    @RequestMapping(value = "update", method = { RequestMethod.POST })
    public ApiResponseEntity update(HttpServletRequest request, ${className} entity) {
        int r = ${classNameLower}Service.update(entity);
        if(r == 0) {
            return ApiResponseEntity.fail("更新失败");
        }
        return ApiResponseEntity.success();
    }

    /**
     * 删除
     *
     * @param request
     * @param entity
     * @return
     */
    @RequestMapping(value = "delete", method = { RequestMethod.POST })
    <#if showSwagger == 'true'>
    @ApiOperation("删除")
    </#if>
    public ApiResponseEntity delete(HttpServletRequest request, ${className} entity) {
        int r = ${classNameLower}Service.delete(entity);
        if(r == 0) {
            return ApiResponseEntity.fail("删除失败");
        }
        return ApiResponseEntity.success();
    }

}
