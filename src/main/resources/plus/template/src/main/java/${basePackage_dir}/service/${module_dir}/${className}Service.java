<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign resultEntityNameLower = resultEntityName?uncap_first>
package ${basePackage}.service.${module};


import com.baomidou.mybatisplus.extension.service.IService;
import ${basePackage}.entity.${module}.${className};

<#include "/java_description.include">
public interface ${className}Service extends IService<${className}> {
}
