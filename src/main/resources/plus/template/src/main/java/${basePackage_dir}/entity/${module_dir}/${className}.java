<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basePackage}.entity.${module};

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

<#include "/java_description.include">
@Data
@JsonInclude(Include.NON_NULL)
<#if showSwagger == 'true'>
@ApiModel(value="${table.remarks}", description="${table.remarks}")
</#if>
@TableName("${table.sqlName}")
public class ${className} extends Model<${className}> implements Serializable {

    private static final long serialVersionUID = 1L;

<#list table.columns as column>
    /**
     * ${column.remarks}
     */
    <#if showSwagger == 'true'>
    @ApiModelProperty(value = "${column.remarks}")
    </#if>
    <#if column.pk>
    @TableId(value = "`${column}`", type = IdType.AUTO)
    private ${column.simpleJavaType} ${column.columnNameLower};
    <#else>
    @TableField(value = "${column}")
    <#if "93" == "${column.sqlType}">
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${column.simpleJavaType} ${column.columnNameLower};
    </#if>
</#list>

}
