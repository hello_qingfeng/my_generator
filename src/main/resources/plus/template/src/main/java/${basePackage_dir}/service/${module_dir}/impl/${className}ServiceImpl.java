<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign resultEntityNameLower = resultEntityName?uncap_first>
package ${basePackage}.service.${module};

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import ${basePackage}.mapper.${module}.${className}Mapper;
import ${basePackage}.entity.${module}.${className};
import ${basePackage}.entity.base.Datagrid;
import ${basePackage}.service.${module}.${className}Service;
import ${basePackage}.service.base.BaseService;
import ${basePackage}.utils.DateUtil;

<#include "/java_description.include">
@Service
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper, ${className}> {
}
