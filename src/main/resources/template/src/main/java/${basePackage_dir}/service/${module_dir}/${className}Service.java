<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign resultEntityNameLower = resultEntityName?uncap_first>
package ${basePackage}.service.${module};

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${basePackage}.mapper.${module}.${className}Mapper;
import ${basePackage}.entity.${module}.${className};
import ${basePackage}.entity.base.Datagrid;
import ${basePackage}.service.${module}.${className}Service;
import ${basePackage}.service.base.BaseService;
import ${basePackage}.utils.DateUtil;

<#include "/java_description.include">
@Service
public class ${className}Service {

    @Autowired
    private ${className}Mapper ${classNameLower}Mapper;

	/**
     * 通过id查询单个对象
     */
    public ${className} queryById(${className} entity){
        return ${classNameLower}Mapper.queryById(entity);
    }

    /**
     * 通过id查询单个对象
     */
    public ${className} queryById(Integer id){
        ${className} entity = new ${className}();
        entity.setId(id);
        return ${classNameLower}Mapper.queryById(entity);
    }

    /**
     * 通过条件查询单个对象
     */
    public ${className} queryOne(${className} entity){
        return ${classNameLower}Mapper.queryOne(entity);
    }

    /**
     * 通过id查询单个对象
     */
    public void queryById(${className} entity, ${resultEntityName} ${resultEntityNameLower}){
        ${resultEntityNameLower}.setData(queryById(entity));
    }

    /**
     * 通过条件查询单个对象
     */
    public void queryOne(${className} entity, ${resultEntityName} ${resultEntityNameLower}){
        datagrid.setData(queryOne(entity));
    }

    /**
     * 查询全部
     */
    public List<${className}> queryList(${className} entity){
        return ${classNameLower}Mapper.queryList(entity);
    }

    /**
     * 查询全部(分页)
     */
    public void queryPageList(${className} entity, ${resultEntityName} ${resultEntityNameLower}){
        List<${className}> list = ${classNameLower}Mapper.queryList(entity);
        ${resultEntityNameLower}.setData(list);
        ${resultEntityNameLower}.setCount(new PageInfo<>(list).getTotal());
    }

    /**
     * 查询总数
     */
    public int queryCount(${className} entity){
        return ${classNameLower}Mapper.queryCount(entity);
    }

    /**
     * 新增
     */
    @Transactional(rollbackFor = Exception.class)
    public int insert(${className} entity)  {
        if (${classNameLower}Mapper.insertSelective(entity) == 0) {
            transactionRollBack();
            return 0;
        }
        return 1;
    }

    /**
     * 批量新增
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertBatch(List<${className}> list) {
        return ${classNameLower}Mapper.insertBatch(list);
    }

    /**
     * 更新
     */
    @Transactional(rollbackFor = Exception.class)
    public int update(${className} entity)  {
        entity.setUpdateTime(new Date());
        if (${classNameLower}Mapper.update(entity) == 0) {
            transactionRollBack();
            return 0;
        }
        return 1;
    }

    /**
     * 删除
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(${className} entity)  {
        entity.setUpdateTime(new Date());
        if (${classNameLower}Mapper.delete(entity) == 0) {
            transactionRollBack();
            return 0;
        }
        return 1;
    }
}
