<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basePackage}.mapper.${module};

import java.util.List;

import ${basePackage}.entity.${module}.${className};

<#include "/java_description.include">
public interface ${className}Mapper extends BaseMapper<${className}> {

	/**
	 * 通过id查询单个对象
     *
     * @param ${classNameLower}
     * @return
     */
    ${className} queryById(${className} ${classNameLower});

	/**
	 * 通过条件查询单个对象
     *
     * @param ${classNameLower}
     * @return
     */
    ${className} queryOne(${className} ${classNameLower});

    /**
     * 查询全部
     *
     * @param ${classNameLower}
     * @return
     */
    List<${className}> queryList(${className} ${classNameLower});

    /**
     * 查询总数
     *
     * @param ${classNameLower}
     * @return
     */
    int queryCount(${className} ${classNameLower});

    /**
     * 新增
     *
     * @param ${classNameLower}
     * @return
     */
    int insert(${className} ${classNameLower});

    /**
     * 可选新增
     *
     * @param ${classNameLower}
     * @return
     */
    int insertSelective(${className} ${classNameLower});

    /**
     * 批量新增
     *
     * @param list
     * @return
     */
    int insertBatch(List<${className}> list);

    /**
     * 更新
     *
     * @param ${classNameLower}
     * @return
     */
    int update(${className} ${classNameLower});

    /**
     * 删除
     *
     * @param ${classNameLower}
     * @return
     */
    int delete(${className} ${classNameLower});

}
