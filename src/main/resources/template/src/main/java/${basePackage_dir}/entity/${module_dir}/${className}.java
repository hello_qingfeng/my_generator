<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basePackage}.entity.${module};

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

<#include "/java_description.include">
@Data
@JsonInclude(Include.NON_NULL)
public class ${className} implements Serializable {

    private static final long serialVersionUID = 1L;

<#list table.columns as column>
    /**
     * ${column.remarks}
     */
    private ${column.simpleJavaType} ${column.columnNameLower};
</#list>

}
