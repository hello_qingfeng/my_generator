<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign resultEntityNameLower = resultEntityName?uncap_first>
package ${basePackage}.controller.${module};

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ${basePackage}.entity.${module}.${className};
import ${basePackage}.service.${module}.${className}Service;
<#if showSwagger == 'true'>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>


<#include "/java_description.include">
<#if showSwagger == 'true'>
@ApiOperation("${controllerRemark}")
</#if>
@RestController
@RequestMapping("${classNameLower}")
public class ${className}Controller {

    @Autowired
    private ${className}Service ${classNameLower}Service;

    /**
     * 查询列表
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("查询列表")
    </#if>
    @RequestMapping(value = "list", method = { RequestMethod.POST, RequestMethod.GET })
    public ${resultEntityName} list(HttpServletRequest request, ${className} entity) {
        ${resultEntityName} ${resultEntityNameLower} = new ${resultEntityName}();
        try {
            ${classNameLower}Service.queryPageList(entity, ${resultEntityNameLower});
        } catch (Exception e) {
        }
        return ${resultEntityNameLower};
    }

    /**
     * 查询单个详情
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("查询单个详情")
    </#if>
    @RequestMapping(value = "info", method = { RequestMethod.POST, RequestMethod.GET })
    public ${resultEntityName} info(HttpServletRequest request, ${className} entity) {
        ${resultEntityName} ${resultEntityNameLower} = new ${resultEntityName}();
        try {
            ${classNameLower}Service.queryById(entity, ${resultEntityNameLower});
        } catch (Exception e) {
            return ${resultEntityName}.error("联系管理员处理");
        }
        return ${resultEntityNameLower};
    }

    /**
     * 添加
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("添加")
    </#if>
    @RequestMapping(value = "insert", method = { RequestMethod.POST })
    public Result insert(HttpServletRequest request, ${className} entity) {
        ${resultEntityName} ${resultEntityNameLower} = new ${resultEntityName}();
        try {
            int r = ${classNameLower}Service.insert(entity);
            if(r == 0) {
                return ${resultEntityName}.error("添加失败");
            }
        } catch (Exception e) {
            return ${resultEntityName}.error("联系管理员处理");
        }
        return datagrid;
    }

    /**
     * 更新
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("更新")
    </#if>
    @RequestMapping(value = "update", method = { RequestMethod.POST })
    public Result update(HttpServletRequest request, ${className} entity) {
        ${resultEntityName} ${resultEntityNameLower} = new ${resultEntityName}();
        try {
            int r = ${classNameLower}Service.update(entity);
            if(r == 0) {
                return ${resultEntityName}.error("更新失败");
            }
        } catch (Exception e) {
            return ${resultEntityName}.error("联系管理员处理");
        }
        return datagrid;
    }

    /**
     * 删除
     *
     * @param request
     * @param entity
     * @return
     */
    <#if showSwagger == 'true'>
    @ApiOperation("删除")
    </#if>
    @RequestMapping(value = "delete", method = { RequestMethod.POST })
    public Result delete(HttpServletRequest request, ${className} entity) {
        try {
            int r = ${classNameLower}Service.delete(entity);
            if(r == 0) {
                return ${resultEntityName}.error("删除失败");
            }
        } catch (Exception e) {
            return ${resultEntityName}.error("联系管理员处理");
        }
        return ${resultEntityName}.success();
    }

}
