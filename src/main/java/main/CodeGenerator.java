package main;

import cn.org.rapid_framework.generator.GeneratorFacade;
import cn.org.rapid_framework.generator.GeneratorProperties;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CodeGenerator {

    public static void main(String[] args) {
        try {
            // 模板地址
            String templatePath = "classpath:plus/template";
            // 获取系统用户名
            GeneratorProperties.setProperty("userName", "zeqi");
            // 输出目录
            GeneratorProperties.setProperty("outRoot", "./generatorOutput");
            // 移除表名前缀逗号切割 GeneratorProperties.setProperty("tableRemovePrefixes", "a_,b,c_");
            GeneratorProperties.setProperty("tableRemovePrefixes", "");
            // Package
            GeneratorProperties.setProperty("basePackage", "com.lmc");
            // 所属模块
            GeneratorProperties.setProperty("module", "paas");
            //是否生成swagger
            GeneratorProperties.setProperty("showSwagger", "true");
            //公共返回对象名
            GeneratorProperties.setProperty("resultEntityName", "PaasWxUser");
            //当前时间
            GeneratorProperties.setProperty("nowTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            //控制层注释
            GeneratorProperties.setProperty("controllerRemark", "微信用户管理");
            // 需要生成的表名
            String[] tables = {"paas_wx_user"};
            GeneratorFacade generator = new GeneratorFacade();
            generator.getGenerator().addTemplateRootDir(templatePath);
            // 删除历史生成的文件
            generator.deleteOutRootDir();
            System.out.println("===开始处理===");
            // 通过数据库表生成文件
            generator.generateByTable(tables);
            // 自动搜索数据库中的所有表并生成文件
            // generator.generateByAllTable();
            // 按table名字删除文件
            // generator.deleteByTable("table_name", "template");
            System.out.println("===结束处理===");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
